//
//  GoogleMapsSDK.h
//  GoogleMapsSDK
//
//  Created by Alexey Ivankov on 08.09.16.
//  Copyright © 2016 Alexey Ivankov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface GoogleMapsSDK : NSObject

@end
